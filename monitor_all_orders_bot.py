#!/usr/bin/env python2

import sys, os
sys.path.insert(0, "dir_or_path")
root_path = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
sys.path.insert(0, root_path + "/lib")
import settings
settings.init()
settings.root_path = root_path

from dbhelper import * #created as a package
from apihelper import *
from coins import *
from order import *

current_orders = Order("Ethash","America","Ethereum")
print "Getting Orders..."
current_orders.getOrdersAll(True)
current_orders.getMyOrders(True)
print "Calculating Profit for all Orders..."
current_orders.getCurrentProfitOrdersAll(True)

