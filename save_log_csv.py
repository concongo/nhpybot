#!/usr/bin/env python2

import sys, os
import csv

import re


sys.path.insert(0, "dir_or_path")
root_path = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
sys.path.insert(0, root_path + "/lib")

import settings
settings.init()
settings.root_path = root_path

log = open(settings.root_path + '/res/order_handler.log')
row = {}
rows = []
i = -1
for line in log:
    line = line.rstrip()
    if re.search('\d+-\d+-\d+ \d+:\d+:\d+', line) : 
        if i>=0 : 
            rows.append(dict(row))
            for k,v in row.iteritems():
                row[k]="" 
        i = i + 1
        row["date_time"] = re.findall('\d+-\d+-\d+ \d+:\d+:\d+', line)[0]
    if re.search('Order Id =', line) : row["order_id"] = re.findall('[^= ]+$', line)[0]
    if re.search('Profitability % =', line) : row["current_profitability"] = float(re.findall('[^= ]+$', line)[0])
    if re.search('Profit Margin % =', line) : row["current_profitmargin"] = float(re.findall('[^= ]+$', line)[0])
    if re.search('PriceMaxWorker', line) : row["pricemaxworker"] = float(re.findall('[^= ]+$', line)[0])
    if re.search('Order Price', line) : row["price"] = float(re.findall('[^= ]+$', line)[0])
    if re.search('Speed', line) : row["accepted"] = float(re.findall('[^= ]+$', line)[0])
    if re.search('Workers', line) : row["workers"] = int(re.findall('[^= ]+$', line)[0])
    if re.search('Advice', line) :
        if re.search('Increase', line) :
            row["action"] = "Increase"
            row["new_price"] = float(re.findall('[^= ]+$', line)[0])
        else:
            row["action"] = "Decrease"
            row["new_price"] = float(re.findall('[^= ]+$', line)[0])
    if re.search('Estimated new Profit', line) : row["estimated_new_profit"] = float(re.findall('[^> ]+$', line)[0])    
    if re.search('New Profit estimate is less', line) : row["increase_not_commited"] = 1
    
rows.append(dict(row))

file_keys = ["order_id","date_time", "current_profitability", "current_profitmargin", 
        "pricemaxworker", "price","accepted","workers", "action", "new_price", 
        "estimated_new_profit", "increase_not_commited"]
        
with open(settings.root_path + '/res/mycsvfile.csv', 'wb') as f:  
    w = csv.DictWriter(f, file_keys)
    w.writerow(dict((fn,fn) for fn in file_keys))
    w.writerows(rows)


