def init():
    global root_path
    global profit_margin
    global silence
    global nh_commision
    global duration_h
    global increase_step
    global delta_price
    global top_worker_pct
    
    
    silence = True
    root_path = ""
    profit_margin = 0.069
    nh_commision = 0.035
    duration_h = 0.006944444 #10 Min in Days
    increase_step = 0.0001
    delta_price = increase_step*5
    top_worker_pct = 1