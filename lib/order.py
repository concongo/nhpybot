from apihelper import *
from dbhelper import *
from securityhelper import *
from coins import *
from operator import itemgetter
import settings
import random
from time import sleep
from datetime import datetime
import logging


class Order(object):
    
    algo = {
        "Ethash": {"id":20,"unit":1000}, 
        "Equihash": {"id":24,"unit":0}, 
        "Pascal":{"id":25,"unit":0}
    }
    location ={"Europe": 0, "America": 1} 
    timestamp = 0
    algo_id = 0
    location_id = -1
    APIhandler = lambda: None
    #db = lambda: None
    APIkey = []
    param_name = {
        "orders_all": 
                ["accepted_speed", "algo", "alive" , "id", "limit_speed", "price",
                "type", "workers", "timestamp", "location_id"],
        "orders_my":
                ["accepted_speed", "algo", "alive", "btc_avail", "btc_paid",
                "end", "id", "limit_speed", "pool_host", "pool_pass", "pool_port",
                "pool_user", "price", "workers", "type", "timestamp", "location_id"
                ],
        "profit_orders_all":
                ["hr", "current_revenue", "current_cost", "current_profit", "current_profit_ratio", "id", "duration","w_price","timestamp"]
        
        }
    
    
    
    def __init__(self,algo, location, coin):
        self.algo_id = self.algo[algo]["id"]
        self.unit = float(self.algo[algo]["unit"])
        self.algo_name = algo
        self.location_id = self.location[location]
        #print "location :" + str(self.location_id)
        self.APIhandler = APIhelper("https://www.nicehash.com/api")
        self.APIkey = getAPI()
        self.coin = coin
        self.orders_all = []
        self.orders_my = []
        return
    
    def decreaseMyOrderPrice(self,order_id):
        
        parameters = {
            "method": "orders.set.price.decrease", 
            "id":self.APIkey[0],
            "key":self.APIkey[1],
            "location":self.location_id,
            "algo":self.algo_id,
            "order":order_id
        }
        #returning -> [accepted_speed, algo, alive, btc_avail, btc_paid, end, id, limit_speed, pool_host, pool_pass, pool_port,
        # pool_user, price, type, workers]
        
        return self.APIhandler.get(parameters)["result"] #the main json will be store in "result"

    def increaseMyOrderPrice(self, order_id, price):
        
        parameters = {
            "method": "orders.set.price", 
            "id":self.APIkey[0],
            "key":self.APIkey[1],
            "location":self.location_id,
            "algo":self.algo_id,
            "order":order_id,
            "price": price
        }
        return self.APIhandler.get(parameters)["result"]
    
    
    def checkMyOrdersProfitability(self):
        
        self.getMyOrders()
        current_PriceMaxWorker = self.getWAPrice(0.05)
        current_DownStep = float(self.getDownStep())
        
        logging.basicConfig(filename=settings.root_path + '/res/order_handler.log',level=logging.DEBUG)

        if len(self.orders_my) > 0:
            for order in self.orders_my:
                current_Profitability = self.getProfitEst(float(order["accepted_speed"])*float(self.unit), float(order["price"]))
                logging.info(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
                logging.info("Order Id = " + str(order["id"]))
                logging.info("Current Profitability % = " + str(current_Profitability))
                logging.info("Current Profit Margin % = " + str(settings.profit_margin))
                logging.info("Current PriceMaxWorker Price = " + str(current_PriceMaxWorker))
                logging.info("Current Order Price = " + str(order["price"]))
                logging.info("Current Speed Accepted = " + str(order["accepted_speed"]))
                logging.info("Current Workers = " + str(order["workers"]))
                if (current_Profitability < settings.profit_margin):
                    logging.info("Advice: Decrease Price - NewPrice = " + str(float(order["price"])+current_DownStep))
                    advice_price = float(order["price"])+current_DownStep
                    logging.info("Estimated new Profit -> " + str(self.getProfitEst(float(order["accepted_speed"])*self.unit, advice_price)))
                    result = self.decreaseMyOrderPrice(order["id"])
                    try: 
                        logging.info(result["success"])
                    except:
                        logging.warning(result["error"])
                elif (current_Profitability == 1) or (current_PriceMaxWorker > float(order["price"]) or order["workers"]==0):
                    
                    if ((current_PriceMaxWorker - float(order["price"])) > settings.delta_price):
                        price_increase = settings.delta_price
                    else:
                        price_increase = settings.increase_step
                        
                    logging.info("Advice: Increase Price - NewPrice = " + str(float(order["price"])+price_increase))
                    advice_price = float(order["price"])+price_increase
                    new_profit_est = self.getProfitEst(float(order["accepted_speed"])*self.unit, advice_price)
                    logging.info("Estimated new Profit % -> " + str(new_profit_est))
                    if (new_profit_est > current_Profitability) or (int(order["workers"])<2):
                        result = self.increaseMyOrderPrice(order["id"], advice_price)
                        try:
                            logging.info(result["success"])
                        except:
                            logging.warning(result["error"])
                    else:
                        logging.warning("New Profit estimate is less than the actual. We will do nothing!")
        else:
            logging.warning("Nothing to check! No Active Orders!")
    
        return
    
    def getOrdersAll(self, save=False):

        if (save):
            db = Dbhelper(settings.root_path + '/res/coins.sqlite')
            
        parameters = {
            "method": "orders.get", 
            "location":self.location_id,
            "algo":self.algo_id
        }
        #returning -> [accepted_speed, algo, alive, btc_avail, btc_paid, end, id, limit_speed, pool_host, pool_pass, pool_port,
        # pool_user, price, type, workers]
        orders_all = self.APIhandler.get(parameters)["result"] #the main json will be store in "result"
        self.timestamp = orders_all["timestamp"] # come within the Json
        self.orders_all = orders_all["orders"]

        if (save):
            table="orders_all"
            insert_sets = {}
            for order in self.orders_all:
                order["timestamp"] = self.timestamp
                order["location_id"] = self.location_id
                for param in self.param_name["orders_all"]:
                    insert_sets[param]=order[param]
                #print insert_sets
                db.insert(table, insert_sets)
        return
    
    def getMyOrders(self, save=False):
        if (save):
            db = Dbhelper(settings.root_path + '/res/coins.sqlite')

        parameters = {
            "method": "orders.get&my", 
            "id":self.APIkey[0],
            "key":self.APIkey[1],
            "location":self.location_id,
            "algo":self.algo_id
        }
        #returning -> [accepted_speed, algo, alive, btc_avail, btc_paid, end, id, limit_speed, pool_host, pool_pass, pool_port,
        # pool_user, price, type, workers]
        
        orders_my = self.APIhandler.get(parameters)["result"] #the main json will be store in "result"
        
        self.timestamp = orders_my["timestamp"]
        self.orders_my = orders_my["orders"]

        if (save):
            table="orders_my"
            insert_sets = {}
            for order in self.orders_my:
                order["timestamp"] = self.timestamp
                order["location_id"] = self.location_id
                for param in self.param_name["orders_my"]:
                    insert_sets[param]=order[param]
                #print insert_sets
                db.insert(table, insert_sets)
      
        return
       
    def getDownStep(self):
        
        parameters = {
            "method": "buy.info"
        }
        #returning -> [accepted_speed, algo, alive, btc_avail, btc_paid, end, id, limit_speed, pool_host, pool_pass, pool_port,
        # pool_user, price, type, workers]
        self.DownStep = self.APIhandler.get(parameters)["result"]["algorithms"][self.algo_id]["down_step"] #the main json will be store in "result"
        return self.DownStep

        
    def getStatGlobalCurrent(self):
        parameters = {
            "method": "stats.global.current",
            "location": self.location_id
        }
        #info returning -> { profitability_above_eth, price, speed, profitability_eth} 
        return self.APIhandler.get(parameters)["result"]["stats"][self.algo_id]
    
    def getStatGlobal24(self):
        parameters = {
            "method": "stats.global.24h",
            "location": self.location_id
        }
        #info returning -> { profitability_above_eth, price, speed, profitability_eth} 
        return self.APIhandler.get(parameters)["result"]["stats"][self.algo_id]

    def getTopWorkerOrders(self,i):
        
        keyValList = [0] #Filter for Standar Orders only
        standard_orders=list(filter(lambda d: d['type'] in keyValList, self.orders_all))
        number_of_orders = len(standard_orders)

        top_i_standard = sorted(standard_orders, key=itemgetter('workers'), reverse=True)[:int(i*number_of_orders)]
        
        return top_i_standard

    def getProfitEst(self, hr, price, duration=settings.duration_h):
        
        #hr in Mega/Hash
        current_coin = Coin(self.algo_name)
        est_revenue_24h = current_coin.getCoinData(self.coin,"btc_revenue",hr)
        current_revenue = float(est_revenue_24h) * float(duration)
        current_cost = (float(hr)/float(self.unit) * float(duration) * float(price))
        current_profit = float(current_revenue) - float(current_cost)
        if current_revenue > 0 :
            current_profit_ratio = float(current_profit)/float(current_revenue)
        else:
            current_profit_ratio = 0
        return current_profit_ratio
    
    def getCurrentProfitOrdersAll(self, save = False):
    # This function will set all the Mining Values for the orders in one Var
    
        if (save):
            db = Dbhelper(settings.root_path + '/res/coins.sqlite') 
    
        current_coin = Coin(self.algo_name)
        table="profit_orders_all"
        insert_sets = {}
        w_price = self.getWAPrice()
        number_of_orders = len(self.orders_all)
        i = 1
        for order in self.orders_all:
            print "Processing Order " + str(i) + "/" + str(number_of_orders)
            accepted_speed = order["accepted_speed"]
            order["timestamp"] = self.timestamp
            order["location_id"] = self.location_id
            order["hr"] = float(order["accepted_speed"])*float(self.unit)
            order["w_price"] = w_price
            
            if order["hr"] >0 :
                
                est_revenue_24h = current_coin.getCoinData(self.coin,"btc_revenue",order["hr"])
                price = float(order["price"])
                #print "est_revenue_24h " + str(float(est_revenue_24h))
                #print "settings.duration_h " + str(float(settings.duration_h))
                current_revenue = float(est_revenue_24h) * float(settings.duration_h)
                current_cost = (float(accepted_speed) * settings.duration_h * price)
                current_profit = float(current_revenue) - float(current_cost)
                if current_revenue > 0 :
                    current_profit_ratio = float(current_profit)/float(current_revenue)
                else:
                    current_profit_ratio = 0
                
                order["current_revenue"] = current_revenue
                order["current_cost"] = current_cost
                order["current_profit"] = current_profit
                order["current_profit_ratio"] = current_profit_ratio
                order["duration"] = settings.duration_h
                
                if not settings.silence:    
                    print "Current Revenue BTC = " + str(current_revenue)
                    print "Acepted Speed = " + str(accepted_speed)
                    print "Estimated Current Cost = " + str(current_cost)
                    print "Estimated Profit  = " +str(current_profit)
                    
                sleep(random.random()*5)      
            else:
                order["current_revenue"] = 0
                order["current_cost"] = 0
                order["current_profit"] = 0
                order["current_profit_ratio"] = 0
                order["duration"] = settings.duration_h
            
            if (save):
                for param in self.param_name["profit_orders_all"]:
                    insert_sets[param]=order[param]
                db.insert(table, insert_sets)
            
            i = i + 1    
                
        return

    #def getBestPriceMaxWorker(self):
        #get the Weighted Average of the current timestamp (self.timestamp) and currente location_id
        #if self.timestamp != 0 and self.location_id > -1:
        #    db = Dbhelper(settings.root_path + '/res/coins.sqlite')
        #    rs = db.fetchall("SELECT * FROM `orders_all` WHERE `type`= 0 AND `timestamp` = " + str(self.timestamp) + " AND `location_id`=" + str(self.location_id) + " ORDER BY `workers` DESC LIMIT 10")
        #    prices = []
        #    workers = []
        #    for record in rs:
        #        prices.append(record["price"])
        #        workers.append(record["workers"])
        #    try:
        #        return sum(x * y for x, y in zip(prices, workers)) / sum(workers)
        #    except:
        #        return -1
        #else:
        #    return
    
    def getWAPrice(self, pct = settings.top_worker_pct):
        
        #get the Weighted Average of the current timestamp (self.timestamp) and currente location_id
        orders = self.getTopWorkerOrders(pct)
        prices = []
        workers = []
        
        for order in orders:
            prices.append(float(order["price"]))
            workers.append(int(order["workers"]))

        try:
            return sum(x * y for x, y in zip(prices, workers)) / sum(workers)
        except:
            return -1

    def getWASpeed(self, pct = settings.top_worker_pct):
        
        #get the Weighted Average of the current timestamp (self.timestamp) and currente location_id
        orders = self.getTopWorkerOrders(pct)
        speeds = []
        workers = []
        
        for order in orders:
            speeds.append(float(order["accepted_speed"]))
            workers.append(int(order["workers"]))

        try:
            return sum(x * y for x, y in zip(speeds, workers)) / sum(workers)
        except:
            return -1
    
    def getProfitAverage(self):
        orders = self.getTopWorkerOrders(settings.top_worker_pct)
        profits = []
        for order in orders:
            profits.append(self.getProfitEst(order["accepted_speed"], order["price"]))
            
        return sum(profits)/len(profits)
