import sqlite3
import sys, os
import time
import settings

##############
# methods: close, query, fetchall, fetchone, lastrowid, update, insert
##############

class Dbhelper(object):
    silence = False
    conn = lambda: None #create an empty object
    cur = lambda: None

    
    def __init__(self, path): # is in init where we put the parameters
        try:
            self.conn =  sqlite3.connect(path)
            self.conn.row_factory = sqlite3.Row
            self.cur = self.conn.cursor()

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            
    def close(self):
        try:
            self.conn.close()

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
              
    def query(self,sql):
        try:
            self.cur.execute(sql)
            result = self.cur.fetchall()
            return result

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
    
    def fetchall(self,sql):
        try:
            self.cur.execute(sql)
            result = self.cur.fetchall()
            return result

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            
    def fetchone(self,sql):
        try:
            self.cur.execute(sql)
            result = self.cur.fetchone()
            return result

        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            
    def lastrowid(self):
        try:
            return self.cur.lastrowid()
            
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        
    def update(self, table, update_sets, key_field, value_key_field):
    #Just for table with one Primary Key
    
        try:
            sql = "Update " + table + " SET "
            for key, value in update_sets.iteritems():
                sql = sql  + key + " = :" + key + ", "
            update_sets['table']=table
            update_sets['key_field']=key_field
            update_sets['value_key_field']=value_key_field

            sql = sql[:-2] #take out the last two char
            sql = sql + " Where " + key_field + " = :value_key_field "
            self.cur.execute(sql,update_sets)
            self.conn.commit()
            
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)

    def insert(self, table, insert_set):
        
        try:
            sql2 = ""
            sql = "Insert into `" + table + "` ("
            for key, value in insert_set.iteritems(): #iteritems() it's used to go through pair key, value
                sql = sql + "`" + key + "`,"
                sql2 = sql2 + ":" + key+ ","
            sql = sql[:-1] + ") VALUES (" + sql2[:-1] + ")"
            #print sql
            self.cur.execute(sql,insert_set)
            self.conn.commit()
            
            # if not settings.silence: print "(" + str(int(round(time.time() * 1000))) + ") - Record inserted -> Table [" + table + "]"
            return
        
        except Exception as e:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
        