# This Python file uses the following encoding: utf-8

from apihelper import *
from dbhelper import *

class Coin(object):
    
    hr = 0
    name = ''
    tag = ''
    algo = {"Ethash": "eth", "Equihash": "eq", "Pascal":"pas"}
    coins = ["Ethereum", "Nicehash-Ethash", "EthereumClassic", "Expanse"]
    
    param_name = {
        "coin_set": 
                ["lagging", "timestamp", "algorithm" , "block_time", "tag", "block_reward",
                "block_reward24", "last_block", "difficulty", "difficulty24",
                "nethash", "exchange_rate", "exchange_rate24",
                "exchange_rate_vol",
                "exchange_rate_curr",
                "market_cap"],
        "hash_set":
                ["timestamp",
                "tag",
                "hr",
                "estimated_rewards",
                "estimated_rewards24",
                "btc_revenue",
                "btc_revenue24",
                "profitability",
                "profitability24"]
        }
        

    def __init__(self, algoname, hr=None, cost=None, power=None): # --> algoname should be in coins dictionary
        if hr != None:
            self.hr = hr
        else:
            self.hr = 10 #by default
            
        if cost != None: 
            self.cost = cost
        else:
            self.cost = 0
            
        if power != None: 
            self.power = power
        else:
            self.power = 0
            
        self.algoname = algoname

    def saveData(self,coin):
        db = Dbhelper('res/coins.sqlite')
        
        table="historic_data_coin"
        insert_sets = {}
        for param in self.param_name["coin_set"]:
            insert_sets[param]=self.result["coins"][coin][param]
        db.insert(table, insert_sets)
        
        table="historic_data_hash"
        insert_sets = {}
        for param in self.param_name["hash_set"]:
            insert_sets[param]=self.result["coins"][coin][param]
        db.insert(table, insert_sets)
    
    def show_raw(self):
        self.refresh()
        print self.wtmAPI.showdata()
        
    def refresh(self):
        self.wtmAPI = APIhelper("http://whattomine.com/coins.json")
        self.parameters = {
            "utf8": "✓", 
            self.algo[self.algoname]:"true",
            "factor[" + self.algo[self.algoname] + "_hr]":self.hr, 
            "factor[" + self.algo[self.algoname] + "_p]":self.power,
            "factor[cost]":self.cost,
            "sort": "Profitability24",
            "volume" : 0,
            "revenue" : "current",
            "factor[exchanges][]":"poloniex",
            "commit":"Calculate"
        }
        self.result = self.wtmAPI.get(self.parameters) #the main json will be store in "result"
    
    def getCoin(self,coin, hr=None):
        if hr != None: self.hr = hr
        self.refresh()
        return self.result["coins"][coin]
        
    def getCoinData(self,coin,param, hr=None):
        if hr != None: self.hr = hr
        self.refresh()
        self.result["coins"][coin]["hr"]=hr #adding the parameters to the results since it's not returned
        return self.result["coins"][coin][param]

#test -> eth = Coin("Ethash", 100, 0, 0)
# test-> print eth.getCoin("Ethereum")["btc_revenue"]

#algo = Coin("Ethash", 100, 0, 0)
#print algo.getCoinData("Ethereum","btc_revenue",200)