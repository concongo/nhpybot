import base64
import settings

from Crypto.Cipher import AES # encryption library


def getKey():
    textFile = open(settings.root_path + '/res/pk.nb')
    for line in textFile:
        return line.rstrip()

def getAPIkey():
    textFile = open(settings.root_path + '/res/nhk.nb')
    for line in textFile:
        return line.rstrip()

def getAPI():
    return decode(getKey(),getAPIkey()).split("=")

def saveEK(ss):
    textFile = open(settings.root_path + '/res/nhk.nb','w')
    textFile.write(ss)
    textFile.close()
    
def encode(key,text):
    BLOCK_SIZE = 32
    
    # the character used for padding--with a block cipher such as AES, the value
    # you encrypt must be a multiple of BLOCK_SIZE in length.  This character is
    # used to ensure that your value is always a multiple of BLOCK_SIZE
    PADDING = '{'
    
    # one-liner to sufficiently pad the text to be encrypted
    pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
    
    # one-liners to encrypt/encode and decrypt/decode a string
    # encrypt with AES, encode with base64
    EncodeAES = lambda c, s: base64.b64encode(c.encrypt(pad(s)))

    # create a cipher object using the random secret
    cipher = AES.new(key)

    # encode a string
    return EncodeAES(cipher, text)
    
def decode(key,text):

    BLOCK_SIZE = 32
    
    # the character used for padding--with a block cipher such as AES, the value
    # you encrypt must be a multiple of BLOCK_SIZE in length.  This character is
    # used to ensure that your value is always a multiple of BLOCK_SIZE
    PADDING = '{'
    
    # one-liner to sufficiently pad the text to be encrypted
    pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * PADDING
    
    # one-liners to encrypt/encode and decrypt/decode a string
    # encrypt with AES, encode with base64
    DecodeAES = lambda c, e: c.decrypt(base64.b64decode(e)).rstrip(PADDING)
    
    # create a cipher object using the random secret
    cipher = AES.new(key)

    # decode the encoded string
    return DecodeAES(cipher, text)

#saveEK(encode(getKey(),"XXXXXXX=XXXXXXXX"))

#print decode(getKey(),getAPIkey())

#print getAPI()