import urllib
import urllib2
import json

class APIhelper(object):
    
    parameters_encoded = ''
    data = ''
    baseurl = ''
    js = ''
    
    def __init__(self, apiurl):
        #apirul -> URL in the form http://
        #apiparameters -> a dictionary
        self.baseurl = apiurl
       
        
    def get(self, parameters):
        
        
        parameters_encoded = urllib.urlencode(parameters)
        url = self.baseurl + "?" + parameters_encoded
        url = url.replace("%26","&")
        headers = { 'User-Agent' : 'Mozilla/5.0' } #this is important to avoid cloudflare proxy
        req = urllib2.Request(url, None, headers)
        uh = urllib2.urlopen(req)
        #print url
        self.data = uh.read()
        self.js = json.loads(str(self.data))
        return self.js
        
    def showdata(self):
        return json.dumps(self.js, indent=4)
        